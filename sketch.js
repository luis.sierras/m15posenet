let capture;
let posenet;
let noseX,noseY;
let reyeX,reyeY;
let leyeX,leyeY;
let singlePose,skeleton;
let alien;
let alex,smoke;

function setup() {  // this function runs only once while running
    createCanvas(750, 500);
    //console.log("setup funct");
    capture = createCapture(VIDEO);
    capture.hide();

    //load the PoseNet model
    posenet = ml5.poseNet(capture, modelLOADED);
    //detect pose
    posenet.on('pose', recievedPoses);

    alien = loadImage('images/alien.jpg');
    alex = loadImage('images/alex.png');
    amongo = loadImage('images/amonus.png');
    smoke = loadImage('images/cigar.png');
}

function recievedPoses(poses) {
    console.log(poses);

    if(poses.length > 0) {
        singlePose = poses[0].pose;
        skeleton = poses[0].skeleton;
    }
}

function modelLOADED() {
    console.log("model has loaded");
}

/*
function getRandomArbitrary(min, max) { // generate random num
    return Math.random() * (max - min) + min;
}
*/
function draw() { // this function code runs in infinite loop
    
    // images and video(webcam)
    image(capture, 0, 0);
    fill(random(255), random(255), random(255));
    
    if(singlePose) {
        for(let i=0; i<singlePose.keypoints.length; i++) {
            ellipse(singlePose.keypoints[i].position.x, singlePose.keypoints[i].position.y, 20);
        }

        // stroke(random(255), random(255), random(255));
        // strokeWeight(3);

        // for(let j=0; j<skeleton.length; j++) {
        //     line(skeleton[j][0].position.x, skeleton[j][0].position.y, skeleton[j][1].position.x, skeleton[j][1].position.y);
        // }

        //cara
        image(alex, singlePose.nose.x-185, singlePose.nose.y-160, 400, 270);
        //hombros
        image(alien, singlePose.rightShoulder.x-15, singlePose.rightShoulder.y-25, 35, 35);
        image(alien, singlePose.leftShoulder.x-15, singlePose.leftShoulder.y-25, 35, 35);
        //cigarro
        image(smoke, singlePose.nose.x-60, singlePose.nose.y+25, 75, 75);
        //amongus voladores
        image(amongo, singlePose.rightEar.x-150, singlePose.rightEar.y-150, 75, 75);
        image(amongo, singlePose.leftEar.x+150, singlePose.leftEar.y-150, 75, 75);
    }
    
    //background(200);
    //1.point
    point(200, 200);
    //2.line
    //line(200, 200, 300, 300);
    //3.trialgle
    //triangle(100, 200, 300, 400, 150, 250);
    //4.rectangle
    //rect(250, 200, 200, 100);
    //5. circle
    //ellipse(100, 200, 100, 100);
    // color circlw using stroke and crcle
    /*
    fill(127, 102, 34);
    stroke(255, 0, 0);
    ellipse(100, 200, 100, 100);
    stroke(0, 255, 0);
    ellipse(300, 320, 100, 100);
    stroke(0, 0, 255);
    ellipse(400, 400, 100, 100);
    */

    // infite loop using mouse hovering
    //fill(255);
    /*
    r = getRandomArbitrary(0, 255);
    g = getRandomArbitrary(0, 255);
    b = getRandomArbitrary(0, 255);
    fill(r,g,b);
    ellipse(mouseX, mouseY, 50, 50);
    */
   // IMAGE CAPTURE
   //image(capture, 0, 0, 800, 600);
}
